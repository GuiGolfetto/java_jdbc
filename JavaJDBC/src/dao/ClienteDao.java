package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Cliente;

public class ClienteDao {
    private DataSource dataSource;
    
    public ClienteDao(DataSource dataSource){
        this.dataSource = dataSource;
    }
    
    public ArrayList<Cliente> buscarClientes(){
        try{
            String SQL = "SELECT * FROM cliente";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while (rs.next()){
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }
            ps.close();
            return lista;
        }
        catch(SQLException ex){
            System.out.println("erro" + ex.getMessage());
        }return null;
    }

    public Boolean addClient(Cliente cli){
        Boolean retorno;
        try{
            String sql = "INSERT INTO cliente (nome, email, telefone) "
                    + "VALUES (?,?,?)";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(sql);
            ps.setString(1, cli.getNome());
            ps.setString(2, cli.getEmail());
            ps.setString(3, cli.getTelefone());
            
            ps.executeUpdate();
            ps.close();
            retorno = true;
        } catch(SQLException ex){
            ex.printStackTrace();
            retorno = false;
        }
        return retorno;
    }
    
    public Boolean removeClient(int id){
        Boolean retorno;
        try {
            String sql = "DELETE FROM cliente WHERE id = ?";
            PreparedStatement delete = dataSource.getConnection().prepareStatement(sql);
            delete.setInt(1, id);
            
            delete.executeUpdate();
            delete.close();
            retorno = true;
        } catch(SQLException ex){
            ex.printStackTrace();
            retorno = false;
        }
        return retorno;
    }
}
